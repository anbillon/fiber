// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package fiber

import (
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	"gitlab.com/anbillon/fiber/internal/fs"
	"gitlab.com/anbillon/slago"
)

const resourcesDir = "resources"

// AppContext represents the context for the whole lifecycle of fiber application.
type AppContext struct {
	// just wrap some functions of viper
	vp      *viper.Viper
	cliName string
	Engine  ServerEngine
}

// newAppContext creates a new instance of AppContext which can be used to get
// configuration, merge other config or unmarshal configuration to struct.
func newAppContext(engine ServerEngine) *AppContext {
	binPath, err := os.Executable()
	if err != nil {
		slago.Logger().Fatal().Err(err).Msg("create application context fail")
	}

	wd, err := os.Getwd()
	if err != nil {
		slago.Logger().Fatal().Err(err).Msg("create application context fail")
	}

	var binDir string
	var cliName = binPath
	index := strings.LastIndex(binPath, "/")
	if index > 0 {
		cliName = binPath[index+1:]
		binDir = binPath[:index]
	}

	ctx := &AppContext{
		vp:      viper.New(),
		cliName: cliName,
		Engine:  engine,
	}

	// we currently only support yaml config file
	ctx.vp.SetFs(fs.NewResourceFs())
	ctx.vp.SetConfigType("yaml")
	ctx.vp.AddConfigPath(resourcesDir)
	if wd != binDir {
		ctx.vp.AddConfigPath(filepath.Join(filepath.Dir(binPath), resourcesDir))
	}
	// this is used for embedded file system
	ctx.vp.AddConfigPath("/fiber/" + resourcesDir)

	// set default value
	ctx.vp.SetDefault("fiber.application.name", "fiber-app")
	ctx.vp.SetDefault("fiber.server.port", 3000)

	return ctx
}

// commandName returns the command line name of the executed bin.
func (c *AppContext) commandName() string {
	return c.cliName
}

func (c *AppContext) mergeWith(name string) error {
	c.vp.SetConfigName(name)

	if err := c.vp.MergeInConfig(); err != nil {
		return err
	}

	return c.MergeConfigMap(c.vp.AllSettings())
}

// expandMerge expands environment variables and merges config.
func (c *AppContext) expandMerge(cfg map[string]interface{},
	isChild bool) (map[string]interface{}, error) {
	newCfg := make(map[string]interface{})

	for k, v := range cfg {
		switch v.(type) {
		case string:
			newCfg[k] = ExpandEnv(v.(string))

		case map[string]interface{}:
			cc, _ := c.expandMerge(v.(map[string]interface{}), true)
			newCfg[k] = cc

		default:
			newCfg[k] = v
		}
	}

	if isChild {
		return newCfg, nil
	}

	return nil, c.vp.MergeConfigMap(newCfg)
}

// Profile gets current active profile in command line if existed.
func (c *AppContext) Profile() string {
	return c.vp.GetString(keyProfile)
}

// Get returns an interface.
func (c *AppContext) Get(key string) interface{} {
	return c.vp.Get(key)
}

// GetString returns the value associated with the key as a string.
func (c *AppContext) GetString(key string) string {
	return c.vp.GetString(key)
}

// GetBool returns the value associated with the key as a boolean.
func (c *AppContext) GetBool(key string) bool {
	return c.vp.GetBool(key)
}

// GetInt returns the value associated with the key as an integer.
func (c *AppContext) GetInt(key string) int {
	return c.vp.GetInt(key)
}

// GetInt32 returns the value associated with the key as an integer.
func (c *AppContext) GetInt32(key string) int32 {
	return c.vp.GetInt32(key)
}

// GetInt64 returns the value associated with the key as an integer.
func (c *AppContext) GetInt64(key string) int64 {
	return c.vp.GetInt64(key)
}

// GetUint returns the value associated with the key as an unsigned integer.
func (c *AppContext) GetUint(key string) uint {
	return c.vp.GetUint(key)
}

// GetUint32 returns the value associated with the key as an unsigned integer.
func (c *AppContext) GetUint32(key string) uint32 {
	return c.vp.GetUint32(key)
}

// GetUint64 returns the value associated with the key as an unsigned integer.
func (c *AppContext) GetUint64(key string) uint64 {
	return c.vp.GetUint64(key)
}

// GetFloat64 returns the value associated with the key as a float64.
func (c *AppContext) GetFloat64(key string) float64 {
	return c.vp.GetFloat64(key)
}

// GetTime returns the value associated with the key as time.
func (c *AppContext) GetTime(key string) time.Time {
	return c.vp.GetTime(key)
}

// GetDuration returns the value associated with the key as a duration.
func (c *AppContext) GetDuration(key string) time.Duration {
	return c.vp.GetDuration(key)
}

// GetStringSlice returns the value associated with the key as a slice of strings.
func (c *AppContext) GetStringSlice(key string) []string {
	return c.vp.GetStringSlice(key)
}

// GetStringMap returns the value associated with the key as a map of interfaces.
func (c *AppContext) GetStringMap(key string) map[string]interface{} {
	return c.vp.GetStringMap(key)
}

// GetStringMapString returns the value associated with the key as a map of strings.
func (c *AppContext) GetStringMapString(key string) map[string]string {
	return c.vp.GetStringMapString(key)
}

// GetStringMapStringSlice returns the value associated with the key
// as a map to a slice of strings.
func (c *AppContext) GetStringMapStringSlice(key string) map[string][]string {
	return c.vp.GetStringMapStringSlice(key)
}

// UnmarshalStruct unmarshal configuration with `fiber` tag.
func (c *AppContext) UnmarshalStruct(prefix string, rawVal interface{}) error {
	return c.vp.UnmarshalKey(prefix, rawVal, func(c *mapstructure.DecoderConfig) {
		c.TagName = "fiber"
	})
}

// MergeConfigMap merges external configuration map into the configuration of application.
func (c *AppContext) MergeConfigMap(cfg map[string]interface{}) error {
	_, err := c.expandMerge(cfg, false)
	return err
}
