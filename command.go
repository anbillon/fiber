// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package fiber

import (
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"runtime/debug"

	"github.com/spf13/cobra"
	"gitlab.com/anbillon/fiber/internal/banner"
	"gitlab.com/anbillon/slago"
)

var (
	Version      = "dev"
	BuildTime    string
	GitCommit    string
	fiberVersion = "0.0.5"

	keyProfile = "profile"
)

type commandLine struct {
	rootCmd       *cobra.Command
	ctx           *AppContext
	bannerPrinter *banner.Printer
}

func newCommandLine(ctx *AppContext, shortDesc string, bp *banner.Printer) *commandLine {
	rootCmd := &cobra.Command{
		Use:           ctx.commandName(),
		Short:         shortDesc,
		SilenceUsage:  true,
		SilenceErrors: true,
	}

	return &commandLine{
		rootCmd:       rootCmd,
		ctx:           ctx,
		bannerPrinter: bp,
	}
}

// Initialize initializes fiber command line and binds profile flag. This will add
// version command and serve command into root command.
func (c *commandLine) Initialize() {
	serveCmd := c.newServeCmd()
	serveCmd.Flags().StringP(keyProfile, "p", "", "the profile to set")
	err := c.ctx.vp.BindPFlag(keyProfile, serveCmd.Flags().Lookup(keyProfile))
	if err != nil {
		Panicf("initialize command line error %v", err)
	}

	stdOut := c.rootCmd.OutOrStdout()
	c.rootCmd.SetOut(stdOut)
	c.rootCmd.SetErr(stdOut)

	c.rootCmd.AddCommand(c.newVersionCmd(), serveCmd)
}

// Execute executes the root command which will start the aplication.
func (c *commandLine) Execute() error {
	return c.rootCmd.Execute()
}

func (c *commandLine) newVersionCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: fmt.Sprintf("Show the version of %v", c.ctx.commandName()),
		Run: func(_ *cobra.Command, _ []string) {
			fmt.Printf("Version:           %v\n"+
				"Go version:        %v\n"+
				"Git commit:        %v\n"+
				"Built:             %v\n",
				Version, runtime.Version(), GitCommit, BuildTime)
		},
	}
}

func (c *commandLine) newServeCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "serve",
		Short: "Start to serve application",
		RunE: func(_ *cobra.Command, _ []string) error {
			return c.run()
		},
	}
}

func (c *commandLine) run() error {
	defer func() {
		if r := recover(); r != nil {
			slago.Logger().Error().Msgf("%v", string(debug.Stack()))
		}
	}()

	c.bannerPrinter.Print()

	if err := c.initializeApp(); err != nil {
		return err
	}

	c.captureExit(func() {
		for _, l := range innerContainer().getAppStoptListeners() {
			l.OnAppStop()
		}
	})

	for _, l := range innerContainer().getAppStartListeners() {
		l.OnAppStart()
	}

	engine := c.ctx.Engine
	address := fmt.Sprintf(":%v", c.ctx.GetInt("fiber.server.port"))
	slago.Logger().Info().Msgf("%v server engine is starting with address: [%v]",
		engine.Name(), address)

	return engine.Start(address)
}

func (c *commandLine) initializeApp() error {
	// resovle dependencies
	if err := innerContainer().resolve(c.ctx); err != nil {
		return err
	}

	for _, loader := range innerContainer().configLoaders() {
		if err := loader.Load(c.ctx); err != nil {
			return err
		}
	}

	// unmarshal properties for initialization purpose
	for _, property := range innerContainer().configProperties() {
		if err := c.ctx.UnmarshalStruct(property.Prefix(), property); err != nil {
			return err
		}
	}

	// invoke all initializer
	for _, initializer := range innerContainer().initializers() {
		initializer.Initialize(c.ctx)
	}

	return nil
}

func (c *commandLine) captureExit(stop func()) {
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt, os.Kill)
		// block until a signal is received
		<-c
		stop()
		os.Exit(0)
	}()
}
