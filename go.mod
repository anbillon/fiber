module gitlab.com/anbillon/fiber

go 1.13

require (
	github.com/mitchellh/mapstructure v1.1.2
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.0
	github.com/spf13/afero v1.2.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.5.0
	gitlab.com/anbillon/slago v0.3.0
)

replace github.com/ugorji/go v1.1.4 => github.com/ugorji/go v1.1.7
