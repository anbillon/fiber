// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package fiber

import (
	"fmt"

	"gitlab.com/anbillon/fiber/internal/banner"
)

// Fiber represents an application, it contains command line to start the app.
type Fiber struct {
	cmdline *commandLine
}

type builder struct {
	engine      ServerEngine
	description string
	bannerText  string
}

// NewFiberBuilder creates a new builder to build Fiber.
func NewFiberBuilder() *builder {
	return &builder{}
}

func (b *builder) Engine(engine ServerEngine) *builder {
	b.engine = engine
	return b
}

func (b *builder) Description(desc string) *builder {
	b.description = desc
	return b
}

func (b *builder) Banner(text string) *builder {
	b.bannerText = text
	return b
}

// Build builds a new instance of Fiber which will initialize command of
// given Application and configuration.
func (b *builder) Build() *Fiber {
	if b.engine == nil {
		panic("no ServerEngine found, must be added in builder")
	}

	if len(b.bannerText) == 0 {
		b.bannerText = "fiber"
	}

	cli := newCommandLine(newAppContext(b.engine), b.description,
		banner.NewPrinter(b.bannerText, fiberVersion))
	cli.Initialize()

	return &Fiber{
		cmdline: cli,
	}
}

// A convenient func to build fiber and execute the command.
func (b *builder) Run() {
	if err := b.Build().Execute(); err != nil {
		fmt.Println(newAppStartError(err))
	}
}

// Execute the command in fiber and start application finally.
func (f *Fiber) Execute() error {
	return f.cmdline.Execute()
}
