// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package fs

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/spf13/afero"
)

var (
	filename = "/fiber/resources/application.yml"
)

func TestResourceFs(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "resource fs test")
}

var _ = Describe("", func() {
	It("bad base64", func() {
		err := InitResourceFs(filename, "bad base64")
		Expect(err).NotTo(BeNil())
	})
	It("read file", func() {
		err := InitResourceFs(filename,
			"ZmliZXI6CiAgYXBwbGljYXRpb246CiAgICBuYW1lOiBkZW1v")
		Expect(err).To(BeNil())

		resourceFs := NewResourceFs()
		f, err := afero.ReadFile(resourceFs, filename)
		Expect(err).To(BeNil())
		Expect("fiber:\n  application:\n    name: demo").To(Equal(string(f)))

		isDir, _ := afero.IsDir(resourceFs, "/fiber/resources")
		Expect(isDir).To(BeTrue())
	})
})
