// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package fs

import (
	"encoding/base64"
	"os"
	"syscall"
	"time"

	"github.com/spf13/afero"
)

// ResourceFs defines a readonly afero.Fs which is used to read config in resources
// of application. The os filesystem has high priority to read. The embedded
// filesystem will be read if os filesystem not found.
type ResourceFs struct {
	osFs  afero.Fs
	memFs afero.Fs
}

// memFs will be used to store embedded resources
var memFs = afero.NewMemMapFs()

// NewResourceFs creates a new instance ResourceFs.
func NewResourceFs() afero.Fs {
	return &ResourceFs{
		osFs:  afero.NewOsFs(),
		memFs: afero.NewReadOnlyFs(memFs),
	}
}

// InitResourceFs will initialize resource filesystem with given base64 of config file.
func InitResourceFs(name string, fileBase64 string) error {
	file, err := memFs.OpenFile(name, os.O_CREATE, os.ModePerm)
	if err != nil {
		return err
	}

	// decode the base64 to get real file content
	bytes, err := base64.StdEncoding.DecodeString(fileBase64)
	if err != nil {
		return err
	}

	_, err = file.Write(bytes)

	return err
}

func (*ResourceFs) Create(name string) (afero.File, error) {
	return nil, syscall.EPERM
}

func (*ResourceFs) Mkdir(name string, perm os.FileMode) error {
	return syscall.EPERM
}

func (*ResourceFs) MkdirAll(path string, perm os.FileMode) error {
	return syscall.EPERM
}

func (r *ResourceFs) Open(name string) (afero.File, error) {
	file, err := r.osFs.Open(name)
	if err == nil {
		return file, nil
	}

	return r.memFs.Open(name)
}

func (r *ResourceFs) OpenFile(name string, flag int, perm os.FileMode) (afero.File, error) {
	file, err := r.osFs.OpenFile(name, flag, perm)
	if err == nil {
		return file, nil
	}

	return r.memFs.OpenFile(name, flag, perm)
}

func (*ResourceFs) Remove(name string) error {
	return syscall.EPERM
}

func (*ResourceFs) RemoveAll(path string) error {
	return syscall.EPERM
}

func (*ResourceFs) Rename(oldname, newname string) error {
	return syscall.EPERM
}

func (r *ResourceFs) Stat(name string) (os.FileInfo, error) {
	fileInfo, err := r.osFs.Stat(name)
	if err == nil {
		return fileInfo, nil
	}

	return r.memFs.Stat(name)
}

func (*ResourceFs) Name() string {
	return "ResourceFs"
}

func (*ResourceFs) Chmod(name string, mode os.FileMode) error {
	return syscall.EPERM
}

func (*ResourceFs) Chtimes(name string, atime time.Time, mtime time.Time) error {
	return syscall.EPERM
}
