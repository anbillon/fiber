// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package banner

import (
	"fmt"
	"strings"
)

type letterInfo struct {
	lineNum  int
	maxWidth int
}

// Printer represents the bannber printer type.
type Printer struct {
	target       string
	fiberVersion string
	asciiArtMap  map[string]string
}

// NewPrinter creates a new instance of bannber printer.
func NewPrinter(text string, fiberVersion string) *Printer {
	return &Printer{
		target:       text,
		fiberVersion: fiberVersion,
		asciiArtMap:  asciiFonts,
	}
}

// Print prints the banner to stdout.
func (bp *Printer) Print() {
	text := bp.target
	bannerMaxHeight := 0
	for _, letter := range text {
		_, letterInfo := bp.processLetter(string(letter))
		if letterInfo.lineNum > bannerMaxHeight {
			bannerMaxHeight = letterInfo.lineNum
		}
	}

	var bannerLine = ""
	for i := 0; i < bannerMaxHeight-1; i++ {
		for _, v := range text {
			lines, letterInfo := bp.processLetter(string(v))
			if letterInfo.lineNum <= i {
				bannerLine += bp.padRight("", letterInfo.maxWidth)
			} else {
				bannerLine += bp.padRight(lines[i], letterInfo.maxWidth)
			}
		}

		bannerLine += "\n"
	}

	bannerLine += fmt.Sprintf(">> fiber >======< v%v <<\n", bp.fiberVersion)

	fmt.Println(bannerLine)
}

func (bp *Printer) processLetter(letter string) ([]string, letterInfo) {
	var asciiArt = bp.asciiArtMap["?"]
	if art, ok := bp.asciiArtMap[letter]; ok {
		asciiArt = art
	}

	lines := strings.Split(asciiArt, "\n")
	maxWidth := 0
	for i, line := range lines {
		if len(line) > maxWidth {
			maxWidth = len(line)
		}
		lines[i] = line
	}
	return lines, letterInfo{
		lineNum:  len(lines),
		maxWidth: maxWidth,
	}
}

func (bp *Printer) padRight(s string, width int) string {
	if len(s) < width {
		s += strings.Repeat(" ", width-len(s))
	}
	return s
}
