// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package reflectx

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type TestInParam struct {
}

type TestField struct {
	in *TestInParam
}

type FuncType func()

func NewTestField(in TestInParam) *TestField {
	return &TestField{
		in: &in,
	}
}

func NewFuncType() FuncType {
	return func() {
	}
}

func NewFunTypeWithClosure(_ func()) FuncType {
	return func() {
	}
}

func TestReflectionParse(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "reflectx test")
}

var _ = Describe("parse field", func() {
	It("no error", func() {
		field, err := ParseField(&TestField{})
		Expect(err).To(BeNil())
		Expect(*field).To(Equal(Field{
			Package:      "gitlab.com/anbillon/fiber/internal/reflectx",
			Name:         "TestField",
			IsPointer:    true,
			IsCollection: false,
		}))
	})
})

var _ = Describe("parse function", func() {
	It("no error", func() {
		fn, err := ParseFunc(NewTestField)
		Expect(err).To(BeNil())
		Expect(fn.Package).To(Equal("gitlab.com/anbillon/fiber/internal/reflectx"))
		Expect(fn.Name).To(Equal("NewTestField"))
		Expect(fn.FuncType).NotTo(BeNil())
		Expect(fn.FuncValue).NotTo(BeNil())

		Expect(fn.OutParam.Package).To(
			Equal("gitlab.com/anbillon/fiber/internal/reflectx"))
		Expect(fn.OutParam.Name).To(Equal("TestField"))
		Expect(fn.OutParam.IsPointer).To(BeTrue())
	})
	It("no error with func type", func() {
		fn, err := ParseFunc(NewFuncType)
		Expect(err).To(BeNil())
		Expect(fn.OutParam.Package).To(
			Equal("gitlab.com/anbillon/fiber/internal/reflectx"))
		Expect(fn.OutParam.Name).To(Equal("FuncType"))
	})
	It("error", func() {
		_, err := ParseFunc(NewFunTypeWithClosure)
		Expect(err).NotTo(BeNil())
		Expect(err.Error()).To(Equal("cannot parse closure function"))
	})
})
