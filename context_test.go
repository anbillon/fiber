// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package fiber

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type MockServerEngine struct {
}

func (*MockServerEngine) Name() string {
	return "mock"
}

func (*MockServerEngine) Start(address string) error {
	return nil
}

var (
	_ = func() error {
		InitResourceFs("/fiber/resources/application.yml",
			"ZmliZXI6CiAgYXBwbGljYXRpb246CiAgICBuYW1lOiBkZW1v")
		return nil
	}()
	ctx = newAppContext(&MockServerEngine{})
)

func TestAppContext(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "app context test")
}

var _ = Describe("command name", func() {
	It("", func() {
		appConfLoader := &applicationConfigLoader{}
		_ = appConfLoader.Load(ctx)
		Expect(ctx.GetString("fiber.application.name")).To(Equal("demo"))
	})
})
