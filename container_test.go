// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package fiber

import (
	"reflect"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/anbillon/fiber/internal/reflectx"
)

type fooTest struct {
}

type barTest struct {
}

type paramTest struct {
}

type fooDefTest struct {
}

type paramDefTest struct {
}

type primitiveTest struct {
}

type intfTest interface {
	TestIntf()
}

type intfImpl struct {
}

func (*intfImpl) TestIntf() {
}

func newFooTest(_ *paramTest) *fooTest {
	return &fooTest{}
}

func newFooDefTest(_ *paramDefTest) *fooDefTest {
	return &fooDefTest{}
}

func newBarTest(_ *fooTest) *barTest {
	return &barTest{}
}

func newIntfTest() intfTest {
	return &intfImpl{}
}

func newIntfDefaultTest(_ intfTest) *fooTest {
	return &fooTest{}
}

func newPrimitiveTest(_ *string, _ *int, _ chan float32,
	_ map[string]string, _ []string, _ []paramTest) *primitiveTest {
	return &primitiveTest{
	}
}

func mockContainer() *container {
	return &container{
		providers:       make(map[providerKey][]*graphNode),
		unresolvedNodes: make([]*graphNode, 0),
		graphNodes:      make([]*graphNode, 0),
		options:         make(map[string][]*WireOption),
	}
}

func TestContainer(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "fiber container test")
}

var _ = Describe("private func", func() {
	It("build key", func() {
		field, _ := reflectx.ParseField(&fooTest{})
		key := innerContainer().buildKey(field, "")
		Expect(key).To(Equal(providerKey{"gitlab.com/anbillon/fiber.fooTest", "", true}))
	})
	It("build uuid", func() {
		container := mockContainer()
		sUuid := container.buildUuid(&fooTest{})
		fnUuid := container.buildUuid(newFooTest)
		Expect(len(sUuid)).To(Equal(32))
		Expect(sUuid).NotTo(Equal(fnUuid))
	})
})

var _ = Describe("public func", func() {
	It("wire no dependency", func() {
		container := mockContainer()
		container.wire(newFooTest)
		err := container.resolveDependencies()
		Expect(err).NotTo(BeNil())
		Expect(err.Error()).To(Equal(
			"no dependency of *gitlab.com/anbillon/fiber.paramTest " +
				"found for gitlab.com/anbillon/fiber.newFooTest",
		))
	})
	It("wire multiple dependencies", func() {
		container := mockContainer()
		container.wire(&paramTest{}, &paramTest{})
		container.wire(newFooTest)
		err := container.resolveDependencies()
		Expect(err).NotTo(BeNil())
		Expect(err.Error()).To(Equal(
			"more than one dependencies of *gitlab.com/anbillon/fiber.paramTest " +
				"found for gitlab.com/anbillon/fiber.newFooTest"))
	})
	It("wire multiple dependencies with primary", func() {
		container := mockContainer()
		container.wire(&paramTest{})
		container.wireWithOption(&paramTest{}, Primary())
		container.wire(newFooTest)
		err := container.resolveDependencies()
		Expect(err).To(BeNil())
	})
	It("wire with default option", func() {
		container := mockContainer()
		container.wireWithOption(newFooDefTest, Default(&paramDefTest{}))
		err := container.resolveDependencies()
		Expect(err).To(BeNil())
	})
	It("wire with error default option", func() {
		container := mockContainer()
		container.wireWithOption(newFooDefTest, Default(&paramTest{}))
		err := container.resolveDependencies()
		Expect(err).NotTo(BeNil())
		Expect(err.Error()).To(
			Equal("the default value *gitlab.com/anbillon/fiber.paramTest" +
				"\n\tis not match *gitlab.com/anbillon/fiber.paramDefTest" +
				"\n\tin gitlab.com/anbillon/fiber.newFooDefTest"))
	})
	It("wire with func default option", func() {
		container := mockContainer()
		container.wireWithOption(newIntfDefaultTest, Default(newIntfTest()))
		err := container.resolveDependencies()
		Expect(err).To(BeNil())
	})
	It("wire without name", func() {
		container := mockContainer()
		container.wire(&paramTest{})
		container.wireWithOption(newFooTest)
		container.wireWithOption(newBarTest, Name("myfoo"))
		err := container.resolveDependencies()
		Expect(err).NotTo(BeNil())
		Expect(err.Error()).To(Equal(
			"no dependency of *gitlab.com/anbillon/fiber.fooTest(myfoo) " +
				"found for gitlab.com/anbillon/fiber.newBarTest"))
	})
	It("wire with name", func() {
		container := mockContainer()
		container.wire(&paramTest{})
		container.wireWithOption(newFooTest, OutName("foo"))
		container.wireWithOption(newBarTest, Name("foo"))
		err := container.resolveDependencies()
		Expect(err).To(BeNil())
	})
	It("wire with profiles", func() {
		container := mockContainer()
		container.profile = "test"
		container.wireWithOption(newFooTest, Active("dev", "test"))
		container.wire(&paramTest{}, newBarTest)
		err := container.resolveDependencies()
		Expect(err).To(BeNil())
	})
	It("wire with error profiles", func() {
		container := mockContainer()
		container.profile = "dev"
		container.wireWithOption(newFooTest, Active("test"))
		container.wire(&paramTest{}, newBarTest)
		err := container.resolveDependencies()
		Expect(err).NotTo(BeNil())
	})
	It("get struct", func() {
		container := mockContainer()
		container.wire(&paramTest{})
		_ = container.resolveDependencies()

		result := container.retrieve(reflect.TypeOf((*paramTest)(nil)))
		paramTests := make([]*paramTest, 0)
		for _, v := range result {
			paramTests = append(paramTests, v.(*paramTest))
		}
		Expect(1).To(Equal(len(paramTests)))
	})
	It("get interface", func() {
		container := mockContainer()
		container.wire(newIntfTest())
		_ = container.resolveDependencies()

		result := container.retrieve(reflect.TypeOf((*intfTest)(nil)))
		intfTests := make([]intfTest, 0)
		for _, v := range result {
			intfTests = append(intfTests, v.(intfTest))
		}
		Expect(1).To(Equal(len(intfTests)))
	})
	It("wire primitive type", func() {
		container := mockContainer()
		var primitiveStr = "this is string"
		var primitiveInt = 2
		var primitiveChan = make(chan float32)
		var primitiveMap = make(map[string]string)
		var primitiveSlice = make([]string, 0)
		var structSlice = make([]paramTest, 2)
		container.wireWithOption(&primitiveStr, OutName("myString"))
		container.wireWithOption(&primitiveInt, OutName("myInt"))
		container.wireWithOption(primitiveChan, OutName("myChan"))
		container.wireWithOption(primitiveMap, OutName("myMap"))
		container.wireWithOption(primitiveSlice, OutName("mySlice"))
		container.wire(structSlice)
		container.wireWithOption(newPrimitiveTest,
			Name("myString"), Name("myInt"), Name("myChan"),
			Name("myMap"), Name("mySlice"))
		err := container.resolveDependencies()
		Expect(err).To(BeNil())

		result := container.retrieve(reflect.TypeOf((*primitiveTest)(nil)))
		primitiveTests := make([]*primitiveTest, 0)
		for _, v := range result {
			primitiveTests = append(primitiveTests, v.(*primitiveTest))
		}
		Expect(1).To(Equal(len(primitiveTests)))
	})
})
