// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package fiber is a framework to make your golang web/cloud application.
//
// Fiber provides a builtin IoC to wire func/field in your application.
//
// Define ruoters
//
//  type Foo struct {
//      Bar string
//  }
//
//  type HelloRouter struct {
//      foo *Foo
//  }
//
//  func newHelloRouter(foo *Foo) *HelloRouter {
//      return &HelloRouter{
//          foo: foo,
//      }
//  }
//
// Wire
//
// 	fiber.Wire(newHelloRouter, &Foo{})
//
// You can wire the func/struct anywhere you want to. It's recommended to wire them
// in the package init func, and then import the package in main package.
//
// Start application
//
//  func main() {
//	    fiber.NewFiberBuilder().
//		    Engine(engine.NewEchoEngine()).
//		    Description("A fiber example app").
//		    Run()
//  }
//
// If the routers is not in the same package with main, they should be imported.
//
// 	_ "your/app/package/routers"
//
// For more examples, just check fiber-examples project.
package fiber
