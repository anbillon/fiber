# 0.0.5
* Add slago as logging system.
* Add `Primary` and `Active` wire option.
* Fix config loader.

# 0.0.4
* Fix wire option.

# 0.0.3
* Add `WireOption` to support name and required option.
* Add embedded file system worked with fiber-cli.
* Add more unit test with ginkgo.

# 0.0.2
* Reconstruct the whole project with custom IoC.

# 0.0.1
* First implementation for fiber, this is an alpha version.
